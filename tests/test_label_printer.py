# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class LabelPrinterTestCase(ModuleTestCase):
    """Label printer test module"""
    module = 'label_printer'

    @with_transaction()
    def test_type(self):
        """Create printer type"""
        pool = Pool()
        PrinterType = pool.get('label.printer.type')

        type1, = PrinterType.create([{
                    'name': 'Toshiba EX',
                    'communication_mode': 'bartender'
                    }])
        self.assert_(type1.id)

    @with_transaction()
    def test_template(self):
        """Create label template"""
        pool = Pool()
        Template = pool.get('label.template')

        template1, = Template.create([{
                    'name': 'Template 1',
                    'data': [('create', [{'name': 'Data 1', 'value': 'LOT123'}])]
                    }])
        self.assert_(template1.id)

    @with_transaction()
    def test_printer(self):
        """Create printer"""
        pool = Pool()
        Printer = pool.get('label.printer')
        PrinterType = pool.get('label.printer.type')
        Template = pool.get('label.template')

        type1, = PrinterType.create([{
                    'name': 'Toshiba EX',
                    'communication_mode': 'bartender'
                    }])

        template1, = Template.create([{
                    'name': 'Template 1',
                    'data': [('create', [{'name': 'Data 1', 'value': 'LOT123'}])]
                    }])
        printer1, = Printer.create([{
                    'name': 'Printer 1',
                    'type_': type1.id,
                    'template': template1.id,
                    'uri': '192.168.1.100@8000'
                    }])
        self.assert_(printer1.id)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(LabelPrinterTestCase))
    return suite
