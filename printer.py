# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.rpc import RPC

__all__ = ['PrinterType', 'Printer']


class PrinterType(DeactivableMixin, ModelSQL, ModelView):
    """Label printer type"""
    __name__ = 'label.printer.type'

    name = fields.Char('Name', required=True,
                       states={'readonly': ~Eval('active')},
                       depends=['active'])
    communication_mode = fields.Selection(
            [('bartender', 'Bartender'),
             ('colos', 'Colos'),
             ('other', 'Other')], 'Communication mode', required=True,
            states={'readonly': ~Eval('active')},
            depends=['active'])

    @classmethod
    def print_label(cls, printers):
        pool = Pool()
        Printer = pool.get('label.printer')

        if not printers:
            printers = Printer.search()
        for printer in printers:
            printer.type_._print_label(printer)

    def _print_label(self, printer):
        func = getattr(self, '_print_by_%s' % self.communication_mode)
        func(printer)


class Printer(DeactivableMixin, ModelSQL, ModelView):
    """Label printer"""
    __name__ = 'label.printer'

    name = fields.Char('Name', required=True,
        states={'readonly': ~Eval('active')},
        depends=['active'])
    code = fields.Char('Code',
        states={'readonly': ~Eval('active')},
        depends=['active'])
    type_ = fields.Many2One('label.printer.type', 'Type',
        required=True, ondelete='RESTRICT',
        states={'readonly': ~Eval('active')},
        depends=['active'])
    uri = fields.Char('Uri',
        states={'readonly': ~Eval('active')},
        depends=['active'])
    template = fields.Many2One('label.template', 'Label template',
        required=True, states={'readonly': ~Eval('active')},
        depends=['active'])

    @classmethod
    def __setup__(cls):
        super(Printer, cls).__setup__()
        cls._buttons.update({
            'show_data': {}
        })
        cls.__rpc__.update({
                'get_printers_data': RPC(readonly=False)
        })

    @staticmethod
    def default_template():
        Template = Pool().get('label.template')
        templates = Template.search([], limit=2)
        if len(templates) == 1:
            return templates[0].id
        return None

    def get_label_data(self):
        return self.template.get_label_data(self)

    @classmethod
    def show_data(cls, records):
        return cls.get_printers_data()

    @classmethod
    def get_printers_data(cls, grouped_key=('name',), type_=None):
        """Returns a dict with grouped_key as key and a dict of label data as value"""
        _domain = []
        if type_:
            if isinstance(type_, (list, tuple)):
                _domain = [('type_', 'in', type_)]
            else:
                _domain = [('type_', '=', type_)]
        printers = cls.search(_domain)
        res = {}
        for printer in printers:
            _key = tuple([getattr(printer, item, None) for item in grouped_key])
            res.setdefault(_key, None)
            res[_key] = printer.get_label_data()
        return [(k, v) for k, v in res.items()]
